﻿using System;
using ReflectionDemo.Extension;

namespace ReflectionDemo.TestPlugIn
{
    [Serializable]
    public class FirstPlugIn : IReflectionDemoPlugIn
    {
        public string Name { get { return "First demo"; } }
        public string Description { get { return "First demo plugin"; } }
        public int Version { get { return 1; } }
        public int Add(int a, int b)
        {
            return a + b;
        }
    }
}
