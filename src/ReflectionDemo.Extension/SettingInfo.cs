﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace ReflectionDemo.Extension
{
    internal class SettingInfo
    {
        private readonly FieldInfo _fieldInfo;
        private readonly PropertyInfo _propertyInfo;

        public Type Type { get; private set; }
        public string Name { get; private set; }
        public object DefaultValue { get; private set; }


        public SettingInfo(PropertyInfo propertyInfo)
        {
            _propertyInfo = propertyInfo;
            Type = _propertyInfo.PropertyType;
            CreateDefaultValue(_propertyInfo);
            CreateName(_propertyInfo);
        }

        public SettingInfo(FieldInfo fieldInfo)
        {
            _fieldInfo = fieldInfo;
            Type = _fieldInfo.FieldType;
            CreateDefaultValue(_fieldInfo);
            CreateName(_fieldInfo);
        }

        private void CreateDefaultValue(MemberInfo memberInfo)
        {
            var attribute = GetAttribute<DefaultValueAttribute>(memberInfo);
            if (attribute == null)
            {
                DefaultValue = Type == typeof(string) ? "" : Activator.CreateInstance(Type);
            }
            else
            {
                DefaultValue = Convert.ChangeType(attribute.Value, Type);
            }
        }

        private void CreateName(MemberInfo memberInfo)
        {
            var attribute = GetAttribute<SettingAttribute>(memberInfo);
            Name = attribute == null ? "SettingsSection" : attribute.Name ?? memberInfo.Name;
        }

        private T GetAttribute<T>(MemberInfo memberInfo) where T : class
        {
            var attributes = memberInfo.GetCustomAttributes(typeof(T), false).ToArray();
            if (attributes.Length == 0) return null;
            return (T)attributes[0];
        }

        public object GetValue(object obj)
        {
            return _fieldInfo == null ? _propertyInfo.GetValue(obj) : _fieldInfo.GetValue(obj);
        }

        public void SetValue(object obj, object value)
        {
            if (_fieldInfo == null)
            {
                _propertyInfo.SetValue(obj, value);
            }
            else
            {
                _fieldInfo.SetValue(obj, value);
            }

        }
    }
}