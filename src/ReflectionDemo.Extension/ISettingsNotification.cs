﻿namespace ReflectionDemo.Extension
{
    public interface ISettingsNotification : IBeforeLoadSettingsNotification, IAfterLoadSettingsNotification, IBeforeSaveSettingsNotification, IAfterSaveSettingsNotification
    {

    }
    public interface IBeforeLoadSettingsNotification
    {
        void BeforeLoadSettings();
    }
    public interface IAfterLoadSettingsNotification
    {
        void AfterLoadSettings();
    }
    public interface IBeforeSaveSettingsNotification
    {
        void BeforeSaveSettings();
    }
    public interface IAfterSaveSettingsNotification
    {
        void AfterSaveSettings();
    }
}