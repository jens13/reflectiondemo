﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace ReflectionDemo.Extension
{
    public class SettingsManager
    {
        private static ISettingsRepository _settingsRepository;
        public static object InitializeLock = new object();

        public static void Initialize(ISettingsRepository settingsRepository)
        {
            lock (InitializeLock)
            {
                if (_settingsRepository == null)
                {
                    _settingsRepository = settingsRepository;
                }
                else
                {
                    throw new Exception("SettingsManager already initialized.");
                }
            }
        }

        public static void LoadSettings(object obj)
        {
            var typeInfo = obj.GetType().GetTypeInfo();
            var sectionName = GetSectionName(obj);

            var beforeNotification = obj as IBeforeLoadSettingsNotification;
            if (beforeNotification != null) beforeNotification.BeforeLoadSettings();

            foreach (SettingInfo setting in GetSettingsInfos(typeInfo, typeof(SettingAttribute)))
            {
                object value = _settingsRepository.LoadSetting(setting.Name, sectionName, setting.DefaultValue);
                setting.SetValue(obj, value);
            }

            var afterNotification = obj as IAfterLoadSettingsNotification;
            if (afterNotification != null) afterNotification.AfterLoadSettings();
        }

        public static void SaveSettings(object obj)
        {
            var typeInfo = obj.GetType().GetTypeInfo();
            var sectionName = GetSectionName(obj);

            var beforeNotification = obj as IBeforeSaveSettingsNotification;
            if (beforeNotification != null) beforeNotification.BeforeSaveSettings();

            foreach (SettingInfo setting in GetSettingsInfos(typeInfo, typeof(SettingAttribute)))
            {
                object value = setting.GetValue(obj) ?? setting.DefaultValue;
                _settingsRepository.SaveSetting(value, setting.Name, sectionName);
            }

            var afterNotification = obj as IAfterSaveSettingsNotification;
            if (afterNotification != null) afterNotification.AfterSaveSettings();

        }

        private static IEnumerable<SettingInfo> GetSettingsInfos(TypeInfo typeInfo, Type attributeType)
        {
            foreach (var propertyInfo in typeInfo.DeclaredProperties)
            {
                if (!propertyInfo.IsDefined(attributeType, false)) continue;
                yield return new SettingInfo(propertyInfo);
            }
            foreach (var fieldInfo in typeInfo.DeclaredFields)
            {
                if (!fieldInfo.IsDefined(attributeType, false)) continue;
                yield return new SettingInfo(fieldInfo);
            }
        }

        private static string GetSectionName(object obj)
        {
            var baseType = obj.GetType();
            while (baseType != typeof (object) && baseType != null)
            {
                var sectionName = GetSectionNameFromMember(baseType, obj);
                if (!string.IsNullOrWhiteSpace(sectionName)) return sectionName;
                baseType = baseType.BaseType;
            }

            var attributes = obj.GetType().GetCustomAttributes(typeof(SettingsSectionAttribute), true);
            var attribute = attributes.OfType<SettingsSectionAttribute>().FirstOrDefault(a => !string.IsNullOrWhiteSpace(a.Name));
            return attribute == null ? null : attribute.Name;
        }
        private static string GetSectionNameFromMember(Type type, object obj)
        {
            var settings = GetSettingsInfos(type.GetTypeInfo(), typeof(SettingsSectionAttribute)).ToArray();
            if (settings.Length == 1)
            {
                return settings[0].GetValue(obj) as string;
            }
            throw new Exception("SettingsSectionAttribute can only be decorating one field or property per class");
        }

    }
}