﻿using System;

namespace ReflectionDemo.Extension
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Field | AttributeTargets.Property)]
    public class SettingsSectionAttribute : Attribute
    {
        public SettingsSectionAttribute()
        {
            Name = "";
        }

        public SettingsSectionAttribute(string name)
        {
            Name = name;
        }

        public string Name { get; private set; }

    }
}