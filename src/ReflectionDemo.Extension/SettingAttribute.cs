﻿using System;

namespace ReflectionDemo.Extension
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class SettingAttribute : Attribute
    {
        public SettingAttribute()
        {
        }

        public SettingAttribute(string name)
        {
            Name = name;
        }

        public string Name { get; private set; }
    }
}