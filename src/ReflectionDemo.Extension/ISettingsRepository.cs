﻿namespace ReflectionDemo.Extension
{
    public interface ISettingsRepository
    {
        string Name { get; }

        void SaveSetting(object value, string name, string sectionName);
        object LoadSetting(string name, string sectionName, object defaultValue);
    }
}