﻿using System;

namespace ReflectionDemo.Extension
{
    public interface IReflectionDemoPlugIn
    {
        string Name { get; }
        string Description { get; }
        int Version { get; }

        int Add(int a, int b);
    }
}
