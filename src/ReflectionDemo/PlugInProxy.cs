﻿using System;
using System.Linq;
using System.Reflection;
using ReflectionDemo.Extension;

namespace ReflectionDemo
{
    public class PlugInProxy : MarshalByRefObject, IReflectionDemoPlugIn
    {
        private IReflectionDemoPlugIn _plugIn;
        public string Name { get { return _plugIn.Name; } }
        public string Description { get { return _plugIn.Description; } }
        public int Version { get { return _plugIn.Version; } }

        public int Add(int a, int b)
        {
            return _plugIn.Add(a, b);
        }

        public bool LoadPlugIn(string file)
        {
            var assembly = Assembly.LoadFile(file);
            var plugIns = assembly.GetExportedTypes().Where(t => typeof(IReflectionDemoPlugIn).IsAssignableFrom(t)).ToArray();

            if (plugIns.Length != 1) return false;
            _plugIn = (IReflectionDemoPlugIn)Activator.CreateInstance(plugIns[0]);
            return true;
        }
    }
}