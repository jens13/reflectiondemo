﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using ReflectionDemo.Extension;

namespace ReflectionDemo
{
    /// <summary>
    /// Interaction logic for PlugInListUserControl.xaml
    /// </summary>
    public partial class PlugInListUserControl
    {
        private ICollectionView _listItems;

        private ObservableCollection<PlugInWrapper> _observableCollection;

        public event EventHandler<ItemDoubleClickEventArgs> ItemDoubleClick;

        public PlugInListUserControl()
        {
            InitializeComponent();
        }
        public IEnumerable<PlugInWrapper> DataSource
        {
            get
            {
                return _observableCollection.ToArray();
            }
            set
            {
                if (value == null)
                {
                    DataGrid.ItemsSource = null;
                    _observableCollection = null;
                    _listItems = null;
                }
                else
                {
                    _observableCollection = new ObservableCollection<PlugInWrapper>(value);
                    _listItems = CollectionViewSource.GetDefaultView(_observableCollection);
                    DataGrid.ItemsSource = _listItems;
                }
            }
        }
        private void ItemDoubleClicked(object sender, MouseButtonEventArgs e)
        {
            var data = ((DataGridRow)sender).Item as PlugInWrapper;
            if (data == null) return;
            var handler = ItemDoubleClick;
            if (handler == null) return;
            handler(this, new ItemDoubleClickEventArgs(data));
        }
    }

    public class ItemDoubleClickEventArgs : RoutedEventArgs
    {
        public ItemDoubleClickEventArgs(PlugInWrapper item)
        {
            Item = item;
        }

        public PlugInWrapper Item { get; private set; }
    }
}
