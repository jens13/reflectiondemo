﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using ReflectionDemo.Extension;

namespace ReflectionDemo
{
    public class PlugInDomain
    {
        private readonly AppDomain _appDomain;
        private readonly PlugInProxy _plugIn;

        private PlugInDomain(AppDomain appDomain, PlugInProxy plugIn)
        {
            _appDomain = appDomain;
            _plugIn = plugIn;
        }

        public IReflectionDemoPlugIn PlugIn { get { return _plugIn; } }

        public void Unload()
        {
            AppDomain.Unload(_appDomain);
        }

        private static PlugInDomain Load(string plugInPath)
        {
            var appDomain = AppDomain.CreateDomain(Path.GetFileNameWithoutExtension(plugInPath));
            var plugInProxyTypeInfo = typeof(PlugInProxy).GetTypeInfo();
            var plugInProxy = (PlugInProxy)appDomain.CreateInstanceAndUnwrap(plugInProxyTypeInfo.Assembly.FullName, plugInProxyTypeInfo.FullName);

            if (plugInProxy.LoadPlugIn(plugInPath))
            {
                return new PlugInDomain(appDomain, plugInProxy);
            }
            
            AppDomain.Unload(appDomain);
            return null;
        }

        public static IEnumerable<PlugInDomain> LoadPlugIns()
        {
            var path = Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), "PlugIns");
            return Directory.EnumerateFiles(path, "*.dll").Select(Load).Where(pl => pl != null);
        }
    }
}
