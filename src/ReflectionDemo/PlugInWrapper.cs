﻿using System.ComponentModel;
using System.Windows;
using ReflectionDemo.Extension;

namespace ReflectionDemo
{
    public class PlugInWrapper
    {
        [SettingsSection] 
        private string _settingsSection;

        private readonly IReflectionDemoPlugIn _plugIn;

        public PlugInWrapper(IReflectionDemoPlugIn plugIn)
        {
            _plugIn = plugIn;
            _settingsSection = "PlugInWrapper-" + Name;
            SettingsManager.LoadSettings(this);
        }

        public string Name { get { return _plugIn.Name; } }
        public string Description { get { return _plugIn.Description; } }
        public int Version { get { return _plugIn.Version; } }

        [Setting]
        [DefaultValue(1)]
        public int NumberA { get; set; }

        [Setting]
        [DefaultValue(2)]
        public int NumberB { get; set; }

        public void Calculate(Window owner)
        {
            MessageBox.Show(owner, "PlugIn Add: " + _plugIn.Add(NumberA, NumberB));
        }
    }
}