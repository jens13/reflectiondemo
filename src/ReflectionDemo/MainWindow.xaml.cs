﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using ReflectionDemo.Extension;

namespace ReflectionDemo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            SettingsManager.Initialize(new XmlSettingsStorage("settings.xml"));
            Closing+= OnClosing;
        }

        private void OnClosing(object sender, CancelEventArgs cancelEventArgs)
        {
            PlugInList.DataSource.ToList().ForEach(SettingsManager.SaveSettings);
        }

        private void MenuItem_OnClick(object sender, RoutedEventArgs e)
        {
            PlugInList.DataSource = PlugInDomain.LoadPlugIns().Select(p => new PlugInWrapper(p.PlugIn));
        }

        private void PlugInList_OnItemDoubleClick(object sender, ItemDoubleClickEventArgs e)
        {
            e.Item.Calculate(this);
        }
    }
}
