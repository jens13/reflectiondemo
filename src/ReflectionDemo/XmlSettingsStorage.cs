﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Xml.Serialization;
using ReflectionDemo.Extension;

namespace ReflectionDemo
{
    [Serializable]
    public class XmlSettingsStorage : ISettingsRepository
    {
        private readonly CultureInfo _usCultureInfo = CultureInfo.GetCultureInfo("en-US");

        [XmlIgnore]
        private string _filename;

        public XmlSettingsStorage() // Serializer constructor
        {
        }

        public XmlSettingsStorage(string filename)
        {
            ChangeFile(filename);
        }

        public void ChangeFile(string filename)
        {
            _filename = filename;
            if (File.Exists(_filename))
            {
                var serializer = new XmlSerializer(typeof(XmlSettingsStorage));
                var obj = (XmlSettingsStorage)serializer.Deserialize(new StringReader(File.ReadAllText(_filename)));
                Settings = obj.Settings;
            }
            else
            {
                Settings = new List<Setting>();
            }
        }

        public string Name { get; set; }

        public void SaveSetting(object data, string name, string sectionName)
        {
            var setting = Settings.FirstOrDefault(s => s.Name == name && s.SectionName == sectionName);
            if (setting != null)
            {
                Settings.Remove(setting);
            }
            setting = new Setting(name, sectionName, data.GetType().Name, string.Format(_usCultureInfo,"{0}", data));
            Settings.Add(setting);

            var serializer = new XmlSerializer(typeof (XmlSettingsStorage));
            using (var fileStream = File.OpenWrite(_filename))
            {
                serializer.Serialize(fileStream, this);
                fileStream.Flush();
            }
        }

        public object LoadSetting(string name, string sectionName, object defaultValue)
        {
            object value = defaultValue;
            GetSetting(ref value, name, sectionName);
            return value;
        }

        public void SetSetting(object value, string name, string section)
        {
            SaveSetting(value, name, section);
        }

        public void GetSetting(ref object value, string name, string sectionName)
        {
            var setting = Settings.FirstOrDefault(s => s.Name == name && s.SectionName == sectionName);
            if (setting != null)
            {
                var settingValue = value is double ? setting.Value.Replace(",", ".") : setting.Value;
                value = Convert.ChangeType(settingValue, value.GetType(), _usCultureInfo);
            }
        }

        [XmlArray("Settings")]
        [XmlArrayItem("Setting")]
        public List<Setting> Settings { get; set; }

    }
    [Serializable]
    public class Setting
    {
        public Setting()
        {

        }

        public Setting(string name, string sectionName, string type, string value)
        {
            Type = type;
            Value = value;
            SectionName = sectionName;
            Name = name;
        }

        [XmlAttribute]
        public string Name { get; set; }
        [XmlAttribute]
        public string SectionName { get; set; }
        [XmlAttribute]
        public string Type { get; set; }
        [XmlText]
        public string Value { get; set; }
    }
}