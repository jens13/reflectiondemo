﻿using System;
using ReflectionDemo.Extension;

namespace ReflectionDemo.TestPlugInErr
{
    [Serializable]
    public class FirstPlugInErr : IReflectionDemoPlugIn
    {
        public string Name { get { return "First demo Err"; } }
        public string Description { get { return "First demo plugin adding an extra 3"; } }
        public int Version { get { return 1; } }
        public int Add(int a, int b)
        {
            return a + b + 3;
        }
    }
}
